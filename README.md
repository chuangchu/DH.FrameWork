# DH.FrameWork

#### 介绍
基于net core的底层快速开发框架，目前主要net core 版本号为：7.0.100，支持插件模式。

已上传Nuet:请搜索DH.Web.Framework

交流请加群：774046050


1、单例方法可以使用`Singleton<T>.Instance`  
2、支持多语种  
3、支持插件  
4、数据库ORM使用新生命的NewLife.XCode  
5、支持缓存。缓存支持内存缓存和Redis缓存，Redis缓存组件使用新生命的NewLife.Redis  
6、支持项目内事件发布和消费  
7、支持动态路由，目前暂支持首页动态路由为指定插件的页面路由。  
8、支持钉钉和企业微信WebHook  
9、支持基于Json持久化存储的内存数据库  
10、支持虚拟文件系统功能。  
11、支持客如云、银豹平台、微信相关、又拍云相关对接SDK



插件页面性能

![输入图片说明](image.png)